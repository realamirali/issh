# iSSH
iSSH is a dead simple alias manager, specifically designed for managing SSH hosts. It uses a JSON file located at `~/.issh.json` and a master password. All information are encrypted/decrypted using AES algorithim and your master password.
Also, it uses `sshpass` for logging into password-protected hosts. So you need to install `sshpass` in your system.

### Install
**Debian/Debian-based:**
```
sudo apt install sshpass curl
sudo curl -L https://gitlab.com/Ehsaan/issh/raw/master/issh.py -o /usr/local/bin/issh
sudo chmod a+rx /usr/local/bin/issh
issh # initial setup
```

**Fedora:**
```
$ sudo dnf install sshpass curl python2-crypto
$ sudo curl -L https://gitlab.com/Ehsaan/issh/raw/master/issh.py -o /usr/local/bin/issh
$ sudo chmod a+rx /usr/local/bin/issh
$ issh
```