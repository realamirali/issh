#!/usr/bin/env python

import subprocess
import base64
import hashlib
from Crypto import Random
from Crypto.Cipher import AES
import json
import sys
import os
import getpass

# AES Cipher class
class AESCipher(object):

    def __init__(self, key): 
        self.bs = 32
        self.key = hashlib.sha256(key.encode()).digest()

    def encrypt(self, raw):
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]

class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

class DB:
    def __init__( self, file ):
        self.file = file
    
    def quickDecode( self, key, encoded ):
        aes = AESCipher( key )
        return aes.decrypt( encoded )

    def quickEncode( self, key, raw ):
        aes = AESCipher( key )
        return aes.encrypt( raw )
    
    def create( self, master ):
        check_string = AESCipher( master )
        js = json.dumps( [ check_string.encrypt( "Validation String" ), {} ] )
        f = open( self.file, 'w' )
        f.write( js )
        f.close()
    
    def validate( self, given ):
        # Retrive the original encoded string
        f = open( self.file, 'r' )
        js = json.loads( f.readline() )
        f.close()

        original_master = js[0]

        try:
            return self.quickDecode( given, original_master ) == "Validation String"
        except:
            return False

    def getAlias( self, alias ):
        f = open( self.file, 'r' )
        info = json.loads( f.readline() )
        f.close()

        aliases = info[1]
        if not alias in aliases:
            return False

        return aliases[ alias ]

    def pushAlias( self, alias, profile ):
        f = open( self.file, 'r+' )
        info = json.loads( f.readline() )

        aliases = info[1]
        if alias in aliases:
            return False

        info[1][ alias ] = profile
        f.seek( 0 )
        f.write( json.dumps( info ) )
        f.truncate()

        f.close()
        return True

    def removeAlias( self, alias ):
        f = open( self.file, 'r+' )
        info = json.loads( f.readline() )

        del info[1][alias]
        f.seek( 0 )
        f.write( json.dumps( info ) )
        f.truncate()

        f.close()
        return True

    def editAlias( self, alias, profile ):
        f = open( self.file, 'r+' )
        info = json.loads( f.readline() )

        info[1][alias] = profile
        f.seek( 0 )
        f.write( json.dumps( info ) )
        f.truncate()

        f.close()
        return True
        
######################################
if len( sys.argv ) < 3:
    sys.exit( ( color.BOLD + 'Usage: ' + color.END + '%s ' + color.DARKCYAN + '(login|edit|delete)' + color.END + ' ' + color.UNDERLINE + 'alias' + color.END ) % sys.argv[0] )

commands = [ 'login', 'edit', 'delete' ]
command = sys.argv[1]
alias = sys.argv[2]

if not command in commands:
    sys.exit( 'Unknown method' )

#######
db_file = os.path.expanduser( '~/.issh.json' )

if not os.path.isfile( db_file ):
    print( ( color.BOLD + color.PURPLE + "File %s not found, We're going to create it." + color.END ) % db_file )
    try:
        master_password = getpass.getpass( color.BOLD + color.GREEN + "Enter a master password: " + color.END )
    except:
        sys.exit( color.RED + color.BOLD + '\nThe master password is required to decrypt information' + color.END )
    
    db = DB( db_file )
    db.create( master_password )

    print( ( color.BOLD + color.GREEN + "Database file %s has been created with given master password." ) % db_file )
else:
    try:
        master_password = getpass.getpass( color.BOLD + color.GREEN + "Enter your master password: " + color.END )
    except:
        sys.exit( color.RED + color.BOLD + '\nThe master password is required to decrypt information' + color.END )

    db = DB( db_file )

    if not db.validate( master_password ):
        sys.exit( color.BOLD + color.RED + 'Wrong master password. Whaa?' + color.END )

#########

if command == 'login':
    aliasInfo = db.getAlias( alias )
    if not aliasInfo:
        print( ( color.PURPLE + color.BOLD + "%s is not defined. Let's define it!" + color.END ) % alias )
        host_name = str( raw_input( color.GREEN + "Enter the server host name: " + color.END ) )
        username = str( raw_input( color.GREEN + "Enter your username to login: " + color.END ) )
        password = str( getpass.getpass( color.GREEN + "Enter the password (optional): " + color.END ) )
        
        record = [ db.quickEncode( master_password, host_name ), db.quickEncode( master_password, username ), '' ]
        if ( password != '' ):
            record[2] = db.quickEncode( master_password, password )

        db.pushAlias( alias, record ) 
        print( color.GREEN + color.BOLD + "The alias has been saved. You can login now using the same command." + color.END )
        sys.exit( 0 )
    else:
        host_name = db.quickDecode( master_password, aliasInfo[0] )
        username = db.quickDecode( master_password, aliasInfo[1] )
        password = False
        if aliasInfo[2] != '':
            password = db.quickDecode( master_password, aliasInfo[2] )
        
        print( color.BOLD + 'Logging into ' + username + '@' + host_name + color.END )
        
        if password:
            subprocess.call( [ 'sshpass', '-p', password, 'ssh', username + '@' + host_name ] )
        else:
            subprocess.call( [ 'ssh', username + '@' + host_name ] )


if command == 'delete':
    aliasInfo = db.getAlias( alias )
    if not aliasInfo:
        sys.exit( ( color.RED + color.BOLD + "Alias %s not found." ) % alias )
    else:
        db.removeAlias( alias )
        print( ( color.GREEN + color.BOLD + "Alias %s removed from the list." ) % alias )


if command == 'edit':
    aliasInfo = db.getAlias( alias )
    if not aliasInfo:
        sys.exit( ( color.RED + color.BOLD + "Alias %s not found." ) % alias )
    else:
        print( ( color.PURPLE + color.BOLD + "Editing %s. Skiping fields means you want them unchanged." + color.END ) % alias )
        host_name = str( raw_input( color.GREEN + "Enter the server host name: " + color.END ) )
        username = str( raw_input( color.GREEN + "Enter your username to login: " + color.END ) )
        password = str( getpass.getpass( color.GREEN + "Enter the password (optional): " + color.END ) )

        if host_name != '':
            aliasInfo[0] = db.quickEncode( master_password, host_name )
        if username != '':
            aliasInfo[1] = db.quickEncode( master_password, username )
        if password != '':
            aliasInfo[2] = db.quickEncode( master_password, password )
        
        db.editAlias( alias, aliasInfo )
        print( color.GREEN + color.BOLD + "The alias changes has been saved." + color.END )